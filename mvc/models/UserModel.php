<?php
include 'models/BaseModel.php';
/**
 * Created by PhpStorm.
 * User: ngocnh
 * Date: 7/20/15
 * Time: 10:34 AM
 */

class UserModel extends BaseModel {
    public function __construct() {
        parent::__construct();
    }

    public function all() {
        $query = "SELECT * FROM users";
        if ($results = $this->db->query($query)) {
            $users = $results->fetch_all();
            mysqli_close($this->db);
            return $users;
        }

        return false;
    }

    public function find($id) {
        $query = "SELECT * FROM users WHERE id=$id";

        if ($result = $this->db->query($query)) {
            $user = $result->fetch_object();
            mysqli_close($this->db);
            return $user;
        }
    }

    public function create($username, $email, $password) {
        $query = "INSERT INTO users(username, email, password) ";
        $query .= "VALUES('$username', '$email', '$password')";

        $result = $this->db->query($query);
        mysqli_close($this->db);
        return $result;
    }

    public function save($id, $email, $password = false) {
        $query = "UPDATE users SET email='$email'";

        if ($password) {
            $query .= ",password='$password'";
        }

        $query .= " WHERE id=$id";

        if ($result = $this->db->query($query)) {
            mysqli_close($this->db);
            return true;
        }

        mysqli_close($this->db);
        return false;
    }

    public function remove($id) {
        $query = "DELETE FROM users WHERE id=$id";

        if ($result = $this->db->query($query)) {
            mysqli_close($this->db);
            return true;
        }

        mysqli_close($this->db);
        return false;
    }

    public function login($username, $password) {
        $query = "SELECT * FROM users WHERE username='$username' AND password='$password'";

        $results = $this->db->query($query);

        if ($results->num_rows > 0) {
            mysqli_close($this->db);
            return true;
        } else {
            mysqli_close($this->db);
            return false;
        }
    }
}