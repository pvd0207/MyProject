<?php

/**
 * Created by ngocnh.
 * Date: 7/22/15
 * Time: 3:37 PM
 */
class User {
    public $id;
    public $username;
    public $email;

    public function __construct($user = array()) {
        if ($user) {
            $this->setId($user[0]);
            $this->setUsername($user[1]);
            $this->setEmail($user[2]);
        }
    }

    public function setId($id) {
        $this->id = (int) $id;
    }

    public function getId() {
        return $this->id;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function getUsername() {
        return $this->username;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getEmail() {
        return $this->email;
    }
}