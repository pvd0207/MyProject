<?php
/**
 * Created by PhpStorm.
 * User: ngocnh
 * Date: 7/20/15
 * Time: 10:27 AM
 */

class BaseModel {
    private $host = 'localhost';
    private $db_user = 'root';
    private $db_pass = '1234';
    private $db_name = 'php-demo';

    public $db;

    public function __construct() {
        $this->db = mysqli_connect($this->host, $this->db_user, $this->db_pass, $this->db_name);

        if (!$this->db) {
            die('Connection failed: ' . $this->db->connect_error);
        }
    }
}