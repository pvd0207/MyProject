<?php include 'partials/header.php'; ?>
<?php if (isset($error) && $error) { ?>
    <div class="alert alert-danger" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <span class="sr-only">Error:</span>
        <?php echo $error; ?>
    </div>
<?php } ?>
<div class="row">
    <div class="panel col-sm-6 col-sm-offset-3">
        <div class="panel-heading">
            <h4>Login</h4>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" method="post">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input class="form-control" type="text" name="username" id="username" />
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input class="form-control" type="password" name="password" id="password" />
                </div>
                <button type="submit" class="btn btn-sm btn-primary" name="login">login</button>
            </form>
        </div>
    </div>
</div>
<?php include 'partials/footer.php'; ?>