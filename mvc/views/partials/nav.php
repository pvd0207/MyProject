<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="http://localhost/php-learning/mvc">ITSol PHP</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="http://localhost/php-learning/mvc">Home</a></li>
                <?php if ($_SESSION['user']) : ?>
                <li><a href="http://localhost/php-learning/mvc/index.php?controller=user&action=logout">Logout</a></li>
                <?php else : ?>
                <li><a href="http://localhost/php-learning/mvc/index.php?controller=user&action=login">Login</a></li>
                <li><a href="http://localhost/php-learning/mvc/index.php?controller=user&action=register">Register</a></li>
                <?php endif; ?>
            </ul>
        </div><!-- /.nav-collapse -->
    </div><!-- /.container -->
</nav><!-- /.navbar -->
