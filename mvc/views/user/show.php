<?php include 'views/partials/header.php'; ?>
<form class="form-horizontal" method="POST">
    <input type="hidden" name="id" value="<?php echo isset($user) && $user ? $user->id : ''; ?>" />
    <div class="panel panel-default">
        <div class="panel-heading clearfix">
            <h4 class="panel-title pull-left">User Information</h4>

            <div class="pull-right">
                <button type="submit" class="btn btn-sm btn-success" name="save">Save</button>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label for="username" class="col-sm-4 control-label">Username</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="username" name="username"
                        value="<?php echo isset($user) && $user ? $user->username : ''; ?>"
                        readonly />
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-4 control-label">Email</label>
                <div class="col-sm-8">
                    <input type="email" class="form-control" id="email" name="email"
                           value="<?php echo isset($user) && $user ? $user->email : ''; ?>" />
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-4 control-label">Password</label>
                <div class="col-sm-8">
                    <input type="password" class="form-control" id="password" name="password" />
                </div>
            </div>
        </div>
    </div>
</form>
<?php include 'views/partials/footer.php'; ?>
