<?php include 'views/partials/header.php'; ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h4><i class="fa fa-user"></i> Users</h4>
    </div>
    <div class="panel-body">
        <table class="table">
            <thead>
                <tr>
                    <th class="col-sm-1">#</th>
                    <th class="col-sm-4">Username</th>
                    <th class="col-sm-5">Email</th>
                    <th class="col-sm-2">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php if(isset($users) && $users) : ?>
                    <?php foreach ($users as $user) : ?>
                        <tr>
                            <td><?php echo $user->id; ?></td>
                            <td><?php echo $user->username; ?></td>
                            <td><?php echo $user->email; ?></td>
                            <td>
                                <span>
                                    <a href="http://localhost/php-learning/mvc/index.php?controller=user&action=edit&id=<?php echo $user->id; ?>">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                </span>
                                <span>
                                    <a href="http://localhost/php-learning/mvc/index.php?controller=user&action=delete&id=<?php echo $user->id; ?>">
                                        <i class="fa fa-remove"></i>
                                    </a>
                                </span>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else : ?>
                    <tr><td colspan="4">No Data</td></tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>
<?php include 'views/partials/footer.php'; ?>
