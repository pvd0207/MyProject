<?php
session_start();
include_once 'controllers/BaseController.php';
/**
 * Created by PhpStorm.
 * User: ngocnh
 * Date: 7/20/15
 * Time: 10:18 AM
 */

if (isset($_GET['controller']) && $_GET['controller']) {
    $c = $_GET['controller'];
} else {
    $c = 'home';
}

$c = ucfirst($c . 'Controller');

include "controllers/$c.php";
$controller = new $c;

if (isset($_GET['action']) && $_GET['action']) {
    $action = $_GET['action'];
} else {
    $action = 'index';
}

$controller->{$action}();