<?php
include_once 'models/entities/User.php';
include_once 'models/UserModel.php';
/**
 * Created by ngocnh.
 * Date: 7/22/15
 * Time: 3:09 PM
 */
class HomeController extends BaseController {
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $userModel = new UserModel();
        $users = array();

        if ($results = $userModel->all()) {
            foreach ($results as $result) {
                $users[] = new User($result);
            }
        }
        return include_once 'views/user/list.php';
    }
}