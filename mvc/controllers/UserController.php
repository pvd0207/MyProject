<?php
include 'models/UserModel.php';
/**
 * Created by PhpStorm.
 * User: ngocnh
 * Date: 7/20/15
 * Time: 10:33 AM
 */

/**
 * So sánh 2 giá trị bằng nhau \    ==
 * So sánh 2 giá trị khác nhau \    !=
 *
 * So sánh 2 giá trị bằng nhau và đồng cùng kiểu \         ===
 * So sánh 2 giá trị không bằng nhau và cùng kiểu \        !==
 *
 */

class UserController extends BaseController {
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        echo "User Index";
    }

    public function create() {

    }

    public function edit() {
        if (isset($_GET['id']) && $_GET['id']) {
            $userModel = new UserModel();

            if (isset($_POST['save'])) {
                $password = $_POST['password'] ?: false;
                if ($userModel->save($_POST['id'], $_POST['email'], $password)) {
                    header('Location: http://localhost/php-learning/mvc');
                }
            }

            if ($user = $userModel->find($_GET['id'])) {
                return include_once 'views/user/show.php';
            }
        }

        header('Location: http://localhost/php-learning/mvc');
    }

    public function delete() {
        if (isset($_GET['id']) && $_GET['id']) {
            $userModel = new UserModel();
            $userModel->remove($_GET['id']);
        }

        header('Location: http://localhost/php-learning/mvc');
    }

    public function login() {
        if ($_SESSION['user']) {
            header('Location: http://localhost/php-learning/mvc');
        }
        if (isset($_POST['login'])) {
            $userModel = new UserModel();

            if ($userModel->login($_POST['username'], $_POST['password'])) {
                $_SESSION['user'] = 'logged_in';
                header('Location: http://localhost/php-learning/mvc');
            } else {
                $error = 'Username or password not found.!';
            }
        }

        return include 'views/login.php';
    }

    public function register() {
        if ($_SESSION['user']) {
            header('Location: http://localhost/php-learning/mvc');
        }
        if(isset($_POST['register'])) {
            if ($_POST['password'] === $_POST['confirm_password']) {
                $userModel = new UserModel();

                if ($userModel->create($_POST['username'], $_POST['email'], $_POST['password'])) {
                    header('Location: http://localhost/php-learning/mvc/user/login');
                }
            }
        }

        return include 'views/register.php';
    }

    public function logout() {
        $_SESSION['user'] = false;
        header('Location: http://localhost/php-learning/mvc/user/login');
    }
}