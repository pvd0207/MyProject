<?php
/**
 * Created by PhpStorm.
 * User: ngocnh
 * Date: 7/20/15
 * Time: 10:27 AM
 */

class BaseController {
    public function __construct() {
        if (isset($_GET['controller']) && $_GET['controller'] != 'user'
            && isset($_GET['action']) && $_GET['action'] != 'login'
            && !$_SESSION['user']) {

            header('Location: http://localhost/php-learning/mvc/user/login');
        }
    }

    public function index() {
        return include 'views/home.php';
    }
}